package com.mygdx.game;

import com.badlogic.gdx.Game;

public class MyGdxGame extends Game {
	private MenuScreen menu;
    private GameScreen gameScreen;

    public void create () {
        menu        = new MenuScreen(this);
        gameScreen  = new GameScreen(this);
        setScreen(menu);
    }

    public void changeToMenuScreen() {
        setScreen(menu);
    }

    public void changeToGameScreen() {
        setScreen(gameScreen);
    }



}
