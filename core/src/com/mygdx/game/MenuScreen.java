package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import org.omg.CORBA.Object;

/**
 *
 * MenuScreen
 * Created by weise_000 on 22.03.2016.
 */
public class MenuScreen implements Screen {

    private Stage stage;
    private Table parentTable;
    private Table menuTable;

    private TextButton startGameButton;
    private TextButton statisticsButton;
    private TextButton exitButton;

    private TextureAtlas buttonAtlas;
    private BitmapFont font;
    private Skin skin;

    private MyGdxGame game;

    public MenuScreen(MyGdxGame game) {

        this.game = game;

        stage = new Stage(new ScreenViewport());

        font = new BitmapFont();
        skin = new Skin();

        buttonAtlas = new TextureAtlas(Gdx.files.internal("assets/data/skin/uiskin.atlas"));
        skin.addRegions(buttonAtlas);


        TextButton.TextButtonStyle buttonStyle = new TextButton.TextButtonStyle();
        buttonStyle.font = font;

        parentTable = new Table();
        parentTable.setFillParent(true);

        menuTable = new Table();
        menuTable.setSize(300,500);

        startGameButton     = new TextButton("Start",buttonStyle);
        statisticsButton    = new TextButton("Statistiken",buttonStyle);
        exitButton          = new TextButton("Exit",buttonStyle);


        startGameButton.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                startGameButtonPressed();
                return true;
            }

            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {

            }
        });

        statisticsButton.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                System.exit(1);
                return true;
            }

            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {

            }
        });

        exitButton.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                System.exit(1);
                return true;
            }

            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {

            }
        });

        menuTable.add(startGameButton).padBottom(20);
        menuTable.row();
        menuTable.add(statisticsButton).padBottom(20);
        menuTable.row();
        menuTable.add(exitButton).padBottom(20);
        parentTable.add(menuTable).align(Align.center);
        stage.addActor(parentTable);

    }

    private void startGameButtonPressed() {
        game.changeToGameScreen();
    }

    @Override
    public void show() {

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();





    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
