package com.mygdx.game.gameUserInterface;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygdx.game.GameData;

/**
 * Created by weise_000 on 24.03.2016.
 */
public class GameUI {

    ProgressCounter progressCounter;


    public GameUI(GameData data) {
        progressCounter = new ProgressCounter(data);
    }

    public void update(GameData data) {
        progressCounter.update(data);
    }

    public void draw(ShapeRenderer renderer) {
        progressCounter.draw(renderer);
    }

}
