package com.mygdx.game.gameUserInterface;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Segment;
import com.mygdx.game.GameData;

/**
 * Created by weise_000 on 24.03.2016.
 */
public class ProgressCounter {


    float positionX;
    float positionY;

    float width;
    float height;

    //Seperationline

    Vector2 pointA;

    Vector2 pointB;


    public ProgressCounter(GameData data) {
        float a = data.getCurrentProgress();
        float b = data.getMaxProgress();


        positionX = Gdx.graphics.getWidth()-50;
        positionY = Gdx.graphics.getHeight()/2;

        width = 25;
        height = Gdx.graphics.getHeight()/4;

        float lineHeight = (a/b);

        pointA = new Vector2(positionX,positionY);
        pointB = new Vector2(positionX+width,positionY+width);

    }


    public void update(GameData data) {
        float a = data.getCurrentProgress();
        float b = data.getMaxProgress();

        float lineHeight = height * (a/b);
        pointA.set(positionX,positionY+lineHeight);
        pointB.set(positionX+width,positionY+lineHeight);


    }

    public void draw(ShapeRenderer renderer) {
        renderer.rect(positionX,positionY,width,height);
        renderer.line(pointA.x,pointA.y,pointB.x,pointB.y);
    }

}
