package com.mygdx.game;

import com.badlogic.gdx.InputProcessor;
import com.mygdx.game.gameObjects.GenericProjectile;
import com.mygdx.game.gameObjects.Projectile;

/**
 * Created by weise_000 on 23.03.2016.
 */
public class Input implements InputProcessor {

    GameData data;

    public Input(GameData data) {
        this.data = data;
    }

    @Override
    public boolean keyDown(int keycode) {
        switch(keycode) {
            case 51 :
                data.getPlayer().setVelocityY(data.getPlayerSpeed());
                break;
            case 47 :
                data.getPlayer().setVelocityY(-data.getPlayerSpeed());
                break;
            case 29 :
                data.getPlayer().setVelocityX(-data.getPlayerSpeed());
                break;
            case 32 :
                data.getPlayer().setVelocityX(data.getPlayerSpeed());
                break;
            case 62 :
                data.addProjectile(new GenericProjectile(data.getPlayer().getPositionX(),data.getPlayer().getPositionY(),
                        data.PROJECTILE_SPEED_X, data.PROJECTILE_SPEED_Y, data.PROJECTILE_TYPE_NORMAL_LIFE, data.PROJECTILE_TYPE_NORMAL_DAMAGE));
                break;
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch(keycode) {
            case 51 :
                if(data.getPlayer().getVelocityY() > 0)
                data.getPlayer().setVelocityY(data.PLAYER_STAND_STILL);
                break;
            case 47 :
                if(data.getPlayer().getVelocityY() < 0)
                data.getPlayer().setVelocityY(data.PLAYER_STAND_STILL);
                break;
            case 29 :
                if(data.getPlayer().getVelocityX() < 0)
                data.getPlayer().setVelocityX(data.PLAYER_STAND_STILL);
                break;
            case 32 :
                if(data.getPlayer().getVelocityX() > 0)
                data.getPlayer().setVelocityX(data.PLAYER_STAND_STILL);
                break;
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
