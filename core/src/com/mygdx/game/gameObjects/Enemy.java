package com.mygdx.game.gameObjects;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by weise_000 on 22.03.2016.
 */
public abstract class Enemy extends GameObject {


    Enemy(float x, float y, float velX, float velY, int life) {
        super(x, y, velX, velY, life);
    }

    public abstract void draw(ShapeRenderer renderer);
    public abstract void update(float delta);

}
