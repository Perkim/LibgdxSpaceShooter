package com.mygdx.game.gameObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Not default anymore!
 * Created by weise_000 on 23.03.2016.
 */
public class Player extends GameObject implements GameObjectInterface {

    final private int size = 12;

    public Player(float x, float y, float velX, float velY, int life) {
        super(x, y, velX, velY, life);
        getHitbox().set(x,y,size,size);
    }

    @Override
    public void draw(ShapeRenderer renderer) {
        renderer.end();
        renderer.begin(ShapeRenderer.ShapeType.Line);
        renderer.rect(getHitbox().x,getHitbox().y,getHitbox().width,getHitbox().height);
        renderer.end();
        renderer.begin(ShapeRenderer.ShapeType.Line);
       //renderer.rect(getPositionX(),getPositionY(),size,size);
       //getHitbox().set(getPositionX(),getPositionY(),size,size);
    }

    @Override
    public void update(float delta) {
        setPositionX(getPositionX() + (getVelocityX() * delta));
        setPositionY(getPositionY() + (getVelocityY() * delta));

        if(getPositionX()+size > Gdx.graphics.getWidth()) {
            setPositionX(Gdx.graphics.getWidth() - 12);
        }

        if(getPositionX() < 0) {
            setPositionX(0);
        }

        if(getPositionY()+size > Gdx.graphics.getHeight()) {
            setPositionY(Gdx.graphics.getHeight() - 12);
        }

        if(getPositionY() < 0) {
            setPositionY(0);
        }


        getHitbox().setPosition(getPositionX(),getPositionY());
    }
}
