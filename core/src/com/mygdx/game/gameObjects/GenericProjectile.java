package com.mygdx.game.gameObjects;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by weise_000 on 23.03.2016.
 */
public class GenericProjectile extends Projectile implements GameObjectInterface{

    private final float SIZE = 5;

    public GenericProjectile(float x, float y, float velX, float velY, int life, int damage) {
        super(x, y, velX, velY, life, damage);
        getHitbox().setSize((int)SIZE,(int)SIZE);
    }

    @Override
    public void draw(ShapeRenderer renderer) {
        renderer.rect(getPositionX(),getPositionY(),SIZE,SIZE);
    }

    @Override
    public void update(float delta) {
        setPositionX(getPositionX() + (getVelocityX() * delta));
        setPositionY(getPositionY() + (getVelocityY() * delta));
        getHitbox().setPosition(getPositionX(),getPositionY());
    }
}
