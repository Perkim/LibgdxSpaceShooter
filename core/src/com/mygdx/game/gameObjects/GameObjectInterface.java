package com.mygdx.game.gameObjects;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by weise_000 on 23.03.2016.
 */
interface GameObjectInterface {

    void draw(ShapeRenderer renderer);

    void update(float delta);



}
