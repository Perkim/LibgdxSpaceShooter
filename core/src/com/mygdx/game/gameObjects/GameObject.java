package com.mygdx.game.gameObjects;


import com.badlogic.gdx.math.Rectangle;

/**
 * Created by weise_000 on 23.03.2016.
 */
public class GameObject {

    private float positionX;
    private float positionY;

    private float velocityX;
    private float velocityY;

    private int life;

    private Rectangle hitbox;


    GameObject(float x, float y, float velX, float velY, int life) {
        this.positionX = x;
        this.positionY = y;
        this.velocityX = velX;
        this.velocityY = velY;
        this.life = life;
        hitbox = new Rectangle(x,y,0,0);
    }


    public float getPositionX() {
        return positionX;
    }

    public void setPositionX(float positionX) {
        this.positionX = positionX;
    }

    public float getPositionY() {
        return positionY;
    }

    public void setPositionY(float positionY) {
        this.positionY = positionY;
    }

    public float getVelocityX() {
        return velocityX;
    }

    public void setVelocityX(float velocityX) {
        this.velocityX = velocityX;
    }

    public float getVelocityY() {
        return velocityY;
    }

    public void setVelocityY(float velocityY) {
        this.velocityY = velocityY;
    }

    public Rectangle getHitbox() {
        return hitbox;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public void setHitbox(Rectangle hitbox) {
        this.hitbox = hitbox;
    }
}
