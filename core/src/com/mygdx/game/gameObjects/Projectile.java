package com.mygdx.game.gameObjects;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import java.util.ArrayList;

/**
 *
 * Projectiles, the Rockets you shoot
 *
 * Created by weise_000 on 22.03.2016.
 */
public abstract class Projectile extends GameObject {

    private int damage;
    private ArrayList<Enemy> enemiesHit;

    Projectile(float x, float y, float velX, float velY, int life, int damage) {
        super(x, y, velX, velY, life);
        this.damage = damage;
        enemiesHit = new ArrayList<Enemy>();
    }

    public abstract void draw(ShapeRenderer renderer);
    public abstract void update(float delta);

    public int getDamage() {
        return damage;
    }
    public void setDamage(int damage) {
        this.damage = damage;
    }

    public ArrayList<Enemy> getEnemiesHit() {
        return enemiesHit;
    }
    public void addEnemyHit(Enemy enemy){
        enemiesHit.add(enemy);
    }
}
