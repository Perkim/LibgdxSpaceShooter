package com.mygdx.game.gameObjects;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by weise_000 on 23.03.2016.
 */
public class GenericSpaceship extends Enemy implements GameObjectInterface {

    private final int size = 12;

    public GenericSpaceship(float x, float y, float velX, float velY, int life) {
        super(x, y, velX, velY,life);
        getHitbox().setSize(size, size);
    }

    @Override
    public void draw(ShapeRenderer renderer) {
        renderer.rect(getPositionX(), getPositionY(), size,size);
    }

    @Override
    public void update(float delta) {
        setPositionX(getPositionX() + (getVelocityX() * delta));
        setPositionY(getPositionY() + (getVelocityY() * delta));
        getHitbox().setPosition(getPositionX(), getPositionY());
    }
}
