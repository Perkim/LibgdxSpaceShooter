package com.mygdx.game;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygdx.game.gameObjects.Enemy;
import com.mygdx.game.gameObjects.GenericSpaceship;
import com.mygdx.game.gameObjects.Player;
import com.mygdx.game.gameObjects.Projectile;

import java.util.ArrayList;

/**
 * All gameData will be saved here
 * Created by weise_000 on 22.03.2016.
 */
public class GameData {

    final int PROJECTILE_SPEED_X = 0;
    final int PROJECTILE_SPEED_Y = 100;
    final int ENEMY_SPEED_X = 0;
    final int ENEMY_SPEED_Y = -50;

    final int PLAYER_START_LIFE = 3;
    final int ENEMY_EASY_LIFE = 1;
    final int PROJECTILE_TYPE_NORMAL_LIFE = 1;
    final int PROJECTILE_TYPE_NORMAL_DAMAGE = 1;

    public static int PLAYER_STAND_STILL = 0;

    private int playerSpeed = 75;

    private float enemySpawnTime = 1;
    private ArrayList<Enemy> enemies;
    private ArrayList<Projectile> projectiles;

    ///PROGRESS
    private float maxProgress = 0;
    private float currentProgress = 2;
    private float firstGoal = 10;
    private float secondGoal = 50;
    private float thirdGoal = 100;




    Player player;

    private MyGdxGame game;

    public GameData(MyGdxGame game) {
        enemies     = new ArrayList<Enemy>();
        projectiles = new ArrayList<Projectile>();
        player = new Player(0,0,0,0, PLAYER_START_LIFE);
        this.game =  game;

        //init score

        maxProgress = firstGoal;
        currentProgress = 0;
    }

    public void drawPlayer(ShapeRenderer renderer) {
        player.draw(renderer);
    }

    public void updatePlayer(float delta) {
        player.update(delta);
    }

    public void checkCollision() {
        for(int i = 0;i < projectiles.size(); i++) {
            for(int c = 0; c < enemies.size(); c++) {
                if(enemies.get(c).getHitbox().overlaps(player.getHitbox())) {
                    System.out.println("Player lost 1 life.");
                    player.setLife(player.getLife()-1);
                    enemies.remove(c);
                } else {
                    if (projectiles.get(i).getHitbox().overlaps(enemies.get(c).getHitbox())) {
                        if (!projectiles.get(i).getEnemiesHit().contains(enemies.get(c))) {
                            projectiles.get(i).addEnemyHit(enemies.get(c));
                            projectiles.get(i).setLife(projectiles.get(i).getLife() - PROJECTILE_TYPE_NORMAL_DAMAGE);
                            enemies.get(c).setLife(enemies.get(c).getLife() - projectiles.get(i).getDamage());
                            if (projectiles.get(i).getLife() < 1) {
                                projectiles.remove(i);
                            }
                            if (enemies.get(c).getLife() < 1) {
                                addProgress(enemies.get(c));
                                enemies.remove(c);

                            }
                        }
                        break;
                    }
                }
            }
        }
    }

    private void addProgress(Enemy enemy) {
        currentProgress += 1;
        if(currentProgress == maxProgress) {
            if(maxProgress < 50) {
                currentProgress = 0;
                maxProgress = secondGoal;
            }
            else {
                    currentProgress = 0;
                    maxProgress = thirdGoal;
                }
        }
    }

    public void drawEnemies(ShapeRenderer renderer) {
        for(Enemy e : enemies) {
            e.draw(renderer);
        }
    }

    public void updateEnemies(float delta) {
        for(Enemy e : enemies) {
            e.update(delta);
        }
    }

    public void drawProjectiles(ShapeRenderer renderer) {
        for(Projectile p : projectiles) {
            p.draw(renderer);
        }
    }

    public void updateProjectiles(float delta) {
        for(Projectile p : projectiles) {
            p.update(delta);
        }
    }

    public void enemySpawnTime(float enemySpawnTime, float enemySpawnPositionX, float enemySpawnPositionY) {
        this.enemySpawnTime += enemySpawnTime;
        if(this.enemySpawnTime > 1) {
            addEnemy(new GenericSpaceship(enemySpawnPositionX, enemySpawnPositionY, ENEMY_SPEED_X, ENEMY_SPEED_Y, ENEMY_EASY_LIFE));
            this.enemySpawnTime = 0;
        }
    }

    public void addProjectile(Projectile p) {
        projectiles.add(p);
    }

    public void addEnemy(Enemy e) {
        enemies.add(e);
    }

    public int getPlayerSpeed() {
        return playerSpeed;
    }

    public void setPlayerSpeed(int playerSpeed) {
        this.playerSpeed = playerSpeed;
    }

    public ArrayList<Enemy> getEnemies() {
        return enemies;
    }

    public void setEnemies(ArrayList<Enemy> enemies) {
        this.enemies = enemies;
    }

    public ArrayList<Projectile> getProjectiles() {
        return projectiles;
    }

    public void setProjectiles(ArrayList<Projectile> projectiles) {
        this.projectiles = projectiles;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void changeToMenuScreen() { game.changeToMenuScreen();};

    public float getCurrentProgress() {
        return currentProgress;
    }


    public float getMaxProgress() {
        return maxProgress;
    }

    public void setMaxProgress(float maxProgress) {
        this.maxProgress = maxProgress;
    }

    public void setCurrentProgress(float currentProgress) {
        this.currentProgress = currentProgress;
    }
}
