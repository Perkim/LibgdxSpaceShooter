package com.mygdx.game;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygdx.game.gameUserInterface.GameUI;

/**
 * The GameScreen, where play happens
 * Created by weise_000 on 22.03.2016.
 */
public class GameScreen implements Screen {

    private GameData gameData;
    private Input input;
    private ShapeRenderer renderer;
    private GameUI gameUI;

    public GameScreen(MyGdxGame game) {

        renderer = new ShapeRenderer();
        gameData = new GameData(game);
        input = new Input(gameData);
        gameUI = new GameUI(gameData);

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(input);
    }

    @Override
    public void render(float delta) {
        Gdx.graphics.getGL20().glClearColor(0,0,0,1);
        Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
        gameData.enemySpawnTime(Gdx.graphics.getDeltaTime(), new Random().nextInt(Gdx.graphics.getWidth()), Gdx.graphics.getHeight());

        gameData.updateEnemies(delta);
        gameData.updatePlayer(delta);
        gameData.updateProjectiles(delta);
        gameData.checkCollision();
        gameUI.update(gameData);

        renderer.begin(ShapeRenderer.ShapeType.Filled);
        gameData.drawEnemies(renderer);
        gameData.drawPlayer(renderer);
        gameData.drawProjectiles(renderer);
        gameUI.draw(renderer);
        renderer.end();


    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
